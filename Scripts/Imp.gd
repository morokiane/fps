extends CharacterBody3D

@onready var sprite: Sprite3D = $Sprite3D
@onready var collision: CollisionShape3D = $CollisionShape3D
@onready var anim: AnimationPlayer = $AnimationPlayer

@export var moveSpeed: float = 4
@export var attackRange: float = 2

@export var animCol: int = 0

var dead: bool = false
var foundPlayer: bool = false
var player: CharacterBody3D

func _ready():
	player = GameController.player
	anim.play("idle")

func _physics_process(_delta):
	if dead:
		return
	if player == null || !foundPlayer:
		return

	var direction = player.global_position - global_position
	direction.y = 0
	direction = direction.normalized()

	velocity = direction * moveSpeed

	if velocity != Vector3(0,0,0):
		anim.play("walk")

	if !dead:
		move_and_slide()

func _process(_delta):
	if !foundPlayer:
		var pFWD: Vector3 = -player.global_transform.basis.z
		var fwd: Vector3 = global_transform.basis.z
		var left: Vector3 = global_transform.basis.z

		var lDot: float = left.dot(pFWD)
		var fDot: float = fwd.dot(pFWD)
		var row: int = 0

		sprite.flip_h = false

		if fDot < -0.85:
			row = 0
		elif fDot > 0.85:
			row = 4
		else:
			sprite.flip_h = lDot > 0
			if abs(fDot) < 0.3:
				row = 2
			elif fDot < 0:
				row = 1
			else:
				row = 3

		sprite.frame = animCol + row * 4

func Kill():
	dead = true
	collision.disabled = true
	anim.stop()

func _on_player_detect_body_entered(body):
	if body is Player:
		foundPlayer = true

func _on_player_detect_body_exited(body):
	if body is Player:
		foundPlayer = false
		anim.play("idle")
