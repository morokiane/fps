extends Node
class_name Interactable

#enum keyType {none, RedKey, yellow, blue}
@export_enum("None", "RedKey", "BlueKey", "YellowKey") var keyType: String

@onready var levelController = get_node("/root/Level")

@export_enum("Lever", "RedLight", "GreenLight") var switchType: String
#@export var requiresKey: bool = false
#@export var KeyType: keyType
@onready var lever = preload("res://textures/walls-switches/sw3s1.png")
@onready var redLight = preload("res://textures/walls-switches/sw1s1.png")
#this will lose everything if the level is reloaded from the .blend
@export var light: Light3D
@export var door: Node
@export var elevator: Node3D
@export var resettable: bool = false
#@export var lever: StandardMaterial3D

var defaultMat: CompressedTexture2D
var used: bool = false
var canUse: bool = false

func GetInteractionText():
	return "Interact"
	
func Interact():
	if GameController.keys.has(keyType):
		canUse = true

	if !used && switchType == "Lever" && canUse:
		defaultMat = get_parent().get_mesh().get("surface_0/material").get_texture(BaseMaterial3D.TEXTURE_ALBEDO)
		get_parent().get_mesh().get("surface_0/material").set_texture(BaseMaterial3D.TEXTURE_ALBEDO, lever)
		
		if light:
			light.light_energy = 5
		
		if door:
			door.get_node("AnimationPlayer").play("Open")

		print("Interacted with %s" % name)
		
		used = true
		
	elif !used && switchType == "RedLight" && canUse:
		defaultMat = get_parent().get_mesh().get("surface_0/material").get_texture(BaseMaterial3D.TEXTURE_ALBEDO)
		get_parent().get_mesh().get("surface_0/material").set_texture(BaseMaterial3D.TEXTURE_ALBEDO, redLight)

		if light:
			light.light_color = "00ff00"
			
		if door:
			door.get_node("AnimationPlayer").play("Open")
			
		if elevator:
			elevator.get_node("AnimationPlayer").play("down")
		
		print("Interacted with %s" % name)
		
		used = true
	else:
		return
		
	if resettable:
		ResetSwitch()
		
func ResetSwitch():
	await get_tree().create_timer(3).timeout
	get_parent().get_mesh().get("surface_0/material").set_texture(BaseMaterial3D.TEXTURE_ALBEDO, defaultMat)
	
	if switchType == "RedLight":
		light.light_color = "ff0000"
	
	if resettable:
		used = false
