extends Area3D

@onready var anim: AnimationPlayer = $"../../AnimationPlayer"
@onready var timer: Timer = $"../../Timer"
@onready var collision: CollisionShape3D = $CollisionShape3D

var down: bool = false

func _ready():
	anim.play("RESET")
	
func _on_body_entered(body):
	if body is Player:
		if !down:
			anim.play("down")
			down = true
		else:
			anim.play("up")
			down = false

func StartTime():
	timer.start()

func _on_timer_timeout():
	anim.play("up")
#	collision.disabled = true
	await get_tree().create_timer(5).timeout
#	collision.disabled = false
	
