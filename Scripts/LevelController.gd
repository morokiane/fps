extends Node

@onready var WE: WorldEnvironment = $WorldEnvironment
@onready var anim: AnimationPlayer = $AnimationPlayer

@export var player: CharacterBody3D

var keys := {}

var dark: bool = false

func _ready():
	anim.play("RESET")

func _on_area_3d_body_entered(body):
	if body is Player && !dark:
		anim.play("worldlight")
		WE.environment.volumetric_fog_enabled = false
		player.find_child("Weapon").modulate = "1c1c1c"
		WE.environment.background_energy_multiplier = 0
		dark = true
	else:
		anim.play_backwards("worldlight")
		player.find_child("Weapon").modulate = "ffffff"
		WE.environment.background_energy_multiplier = 1
		WE.environment.volumetric_fog_enabled = true
		dark = false
