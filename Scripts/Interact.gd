extends RayCast3D

func _process(_delta):
	var collider = self.get_collider()
	
	if self.is_colliding():
		if collider.is_in_group("Interactable"):
			$InteractUI.show()
			if Input.is_action_just_pressed("interact"):
				collider.interact()
	else:
		$InteractUI.hide()
