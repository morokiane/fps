extends Sprite3D

@export var gun: bool = false

@onready var pistolSprite = preload("res://textures/sprites/pista0.png")

func _ready():
	if gun:
		self.texture = pistolSprite
	
func _on_area_3d_body_entered(body):
	if body is Player:
		CheckForWeapon()
	
func CheckForWeapon():
	if GameController.hasPistol == false && gun:
		GameController.hasPistol = true
		GameController.pistolAmmo = 5
		queue_free()
	else:
		GameController.pistolAmmo = 5
		queue_free()
