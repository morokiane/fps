extends CharacterBody3D

@onready var sprite = $AnimatedSprite3D

@export var moveSpeed: float = 2
@export var attackRange: float = 2

var player: CharacterBody3D = GameController.player
var dead: bool = false

func _physics_process(delta):
	if dead:
		return
	if player == null:
		return
	
	var direction = player.global_position - global_position
	direction.y = 0
	direction = direction.normalized()
	
	velocity = direction * moveSpeed
	
	move_and_slide()
	
func Kill()
	pass


#@export var animCol = 0
#@onready var sprite = $Sprite3D

#var cam: Camera3D
#var cam_pos
#var look_point_pos
#var look_point_dir
#var cam_dir
#var angle_to_cam
#var angle_to_lookpoint
#var enemy_angle
#var row: int = 0

#func _ready():
#	cam = get_viewport().get_camera_3d();
#	cam = GameController.player.cam
#	sprite.flip_h = false

#func _process(_delta):
#	if cam == null:
#		return
#
#	#Gets the position of the object the sprite is "looking" at
#	look_point_pos = get_node("LookPoint").global_position
#	look_point_dir = position - look_point_pos
#
#	#Billboards the sprite so it always looks at the player
#	cam_pos = cam.global_transform.origin
#	look_at(cam_pos, Vector3(0, 1, 0))
#	rotation.x = 0
#
#
#	# Calculate the angle of the sprite, taking into consideration both the object the sprite is looking at
#	# and the position of the camera, and convert the radians to degrees for more human friendly readability
#	cam_dir = position - cam_pos
#	angle_to_cam = rad_to_deg(atan2(cam_dir.x, cam_dir.z))
#	angle_to_lookpoint = rad_to_deg(atan2(look_point_dir.x, look_point_dir.z))
#	enemy_angle = angle_to_cam - angle_to_lookpoint
#
#	#Fix negative degrees
#	if enemy_angle < 0:
#		enemy_angle += 360

#	var pFWD: Vector3 = -cam.global_transform.basis.z
#	var fwd: Vector3 = global_transform.basis.z
#	var left: Vector3 = global_transform.basis.z
#
#	var lDot: float = left.dot(pFWD)
#	var fDot: float = fwd.dot(pFWD)
#	var row: int = 0
#	sprite.flip_h = false
#
#	if fDot < -0.85:
#		row = 0
#	elif fDot > 0.85:
#		row = 4
#	else:
#		sprite.flip_h = lDot > 0
#		if abs(fDot) < 0.3:
#			row = 2
#		elif fDot < 0:
#			row = 1
#		else:
#			row = 3
	
#	sprite.frame = animCol + row * 4
