extends CharacterBody3D
class_name Player

@onready var gunRay: RayCast3D = $Head/Camera3d/GunRay
@onready var interact: RayCast3D = $Head/Camera3d/Interact
@onready var cam: Camera3D = $Head/Camera3d
@onready var anim: AnimationPlayer = $AnimationPlayer
@onready var sprite: AnimatedSprite2D = $Head/Camera3d/Weapon
@onready var gunFlash: OmniLight3D = $GunFlash
@onready var flashLight: SpotLight3D = $Head/Camera3d/Flashlight
@onready var ammoHUD: Label = $HUD/Ammo/Amount
@onready var timer: Timer = $Timer
@onready var weapon = $Head/Camera3d/Weapon

#@export var _bullet_scene : PackedScene
##Mouse speed: lower = faster
#@export var mouseSpeed: float = 500
@export var jumpVelocity: float = 6

const bobFreq: float = 1
const bobAmp: float = 0.2
const baseFOV: float = 75
const fovChange: float = 3

var speed: float
var walkSpeed: float = 8
var runSpeed: float = 12
var tBob: float = 0
var mouse_relative_x: float = 0
var mouse_relative_y: float = 0
var moving: bool = false
var running: bool = false
var flashLightOn: bool = false
var canMove: bool = true
var lerpSpeed: float = 10
var canShoot: bool = true

# Get the gravity from the project settings to be synced with RigidBody nodes.
var gravity = ProjectSettings.get_setting("physics/3d/default_gravity")

func _ready():
	GameController.player = self
	#Captures mouse and stops gun from hitting yourself
	gunRay.add_exception(self)
	Input.mouse_mode = Input.MOUSE_MODE_CAPTURED
	weapon.animation_finished.connect(ShootAnimDone)
	
func _process(delta):
	ammoHUD.text = str(GameController.pistolAmmo)
	
	flashLight.light_energy = lerp(flashLight.light_energy, float(GameController.batteryLevel), delta * lerpSpeed)
	if GameController.batteryLevel < 5:
		flashLight.spot_range = lerp(flashLight.spot_range, float(GameController.batteryLevel) + 5, delta * lerpSpeed)

func _input(event):
	if event is InputEventMouseMotion:
		rotation.y -= event.relative.x / GameController.mouseSpeed #left/right turn
		if GameController.invertLook:
			cam.rotation.x += event.relative.y / GameController.mouseSpeed
		else:
			cam.rotation.x -= event.relative.y / GameController.mouseSpeed
			
		cam.rotation.x = clamp(cam.rotation.x, deg_to_rad(-60), deg_to_rad(90) )
		mouse_relative_x = clamp(event.relative.x, -50, 50)
		mouse_relative_y = clamp(event.relative.y, -50, 10)

func _unhandled_input(event):
	if event.is_action_pressed("flashlight") && GameController.hasFlashLight && !flashLightOn && GameController.batteryLevel > 0:
		flashLight.show()
		flashLightOn = true
		timer.start()
	elif event.is_action_pressed("flashlight") && GameController.hasFlashLight && flashLightOn:
		flashLight.hide()
		flashLightOn = false
		timer.stop()

func _physics_process(delta):
	# Add the gravity.
	if not is_on_floor():
		velocity.y -= gravity * delta * 2

	# Handle Jump.
	if Input.is_action_just_pressed("Jump") and is_on_floor():
		velocity.y = jumpVelocity
	# Handle Shooting this can crash if too many of the decal nodes are generated
	if Input.is_action_just_pressed("Shoot") && GameController.pistolAmmo > 0:
		Shoot()
	
	if Input.is_action_pressed("run"):
		speed = runSpeed
	else:
		speed = walkSpeed
	# Get the input direction and handle the movement/deceleration.
	var input_dir = Input.get_vector("moveLeft", "moveRight", "moveUp", "moveDown")
	var direction = (transform.basis * Vector3(input_dir.x, 0, input_dir.y)).normalized()
	
	if is_on_floor():
		if direction:
			velocity.x = lerp(velocity.x, direction.x * speed, delta * 10)
			velocity.z = lerp(velocity.z, direction.z * speed, delta * 10)
		else:
			velocity.x = lerp(velocity.x, direction.x * speed, delta * 7.0)
			velocity.z = lerp(velocity.z, direction.z * speed, delta * 7.0)
	else:
		velocity.x = lerp(velocity.x, direction.x * speed, delta * 3.0)
		velocity.z = lerp(velocity.z, direction.z * speed, delta * 3.0)
		
	tBob += delta * velocity.length() * float(is_on_floor())
	cam.transform.origin = Headbob(tBob)
	
#	var velocityClamped = clamp(velocity.length(), 8.5, runSpeed * 2)
#	var targetFOV = baseFOV + fovChange * velocityClamped
#	cam.fov = lerp(cam.fov, targetFOV, delta * 8.0)
	
	if canMove:
		move_and_slide()

func Headbob(time):
	var pos = Vector3.ZERO
	pos.y = sin(time * bobFreq) * bobAmp + 0.804
	pos.x = cos(time * bobFreq / 2) * bobAmp
	pos.z = -0.302
	return pos


func Shoot():
	if !canShoot || GameController.pistolAmmo == 0:
		return
	canShoot = false
	
	sprite.play("shoot")
	GameController.pistolAmmo -= 1
	if gunRay.is_colliding() && gunRay.get_collider().has_method("Kill"):
		gunRay.get_collider().Kill()
	
func ShootAnimDone():
	canShoot = true
	
func Kill():
	print("pwnd")
	
#func Shoot():
#	if not gunRay.is_colliding():
#		return
#	var bulletInst = _bullet_scene.instantiate() as Node3D
#	bulletInst.set_as_top_level(true)
#	get_parent().add_child(bulletInst)
#	bulletInst.global_transform.origin = gunRay.get_collision_point() as Vector3
#	bulletInst.look_at((gunRay.get_collision_point()+gunRay.get_collision_normal()),Vector3.BACK)
#	print(gunRay.get_collision_point())
#	print(gunRay.get_collision_point()+gunRay.get_collision_normal())
#
#	gunFlash.light_energy = 5
#	await get_tree().create_timer(.05).timeout
#	gunFlash.light_energy = 0
#	GameController.pistolAmmo -= 1
	
func _on_timer_timeout():
	if GameController.batteryLevel > 0:
		GameController.batteryLevel -= 1
	else:
		flashLightOn = false
		flashLight.hide()
