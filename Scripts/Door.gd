extends Node
class_name Door

@onready var timer: Timer = $Timer
@onready var anim: AnimationPlayer = $AnimationPlayer

@export var requireSwitch: bool = false
#@export var requireKey: bool = false
@export_enum("None", "RedKey", "BlueKey", "YellowKey") var keyType: String

var canUse: bool = false

func _ready():
	print(name,": ", "X:", self.position.x," ", "Y:", self.position.y)

func GetInteractionText():
	if requireSwitch || keyType == "None":
		return "Interact"
	
func Interact():
	if GameController.keys.has(keyType):
		canUse = true
		
	if canUse:
		DoorOpen()
	else:
		return

func DoorOpen():
	anim.play("Open")
	timer.start()

func _on_timer_timeout():
	timer.stop()
	anim.play("Close")
	anim.animation_get_next("Pulse")
