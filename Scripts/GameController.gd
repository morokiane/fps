extends Node

#get the player node
var player

var invertLook:bool = true
var fullscreen:bool = false
var mouseReleased: bool = false
var mouseSpeed: float = 500

var hasFlashLight: bool = false
var hasPistol: bool = false
var batteryLevel: int = 10

var pistolAmmo: int = 0
var consoleActive: bool = false

var keys := {}

func _input(event):
	if event.is_action_pressed("mouse") && !mouseReleased:
		Input.mouse_mode = Input.MOUSE_MODE_VISIBLE
		get_tree().paused = true
		mouseReleased = true
	elif event.is_action_pressed("mouse") && mouseReleased:
		Input.mouse_mode = Input.MOUSE_MODE_CAPTURED
		get_tree().paused = false
		mouseReleased = false
		
	if event.is_action_pressed("ui_cancel"):
		get_tree().quit()
	if event.is_action_pressed("fullscreen") && !fullscreen:
		DisplayServer.window_set_mode(DisplayServer.WINDOW_MODE_FULLSCREEN)
		fullscreen = true
	elif event.is_action_pressed("fullscreen") && fullscreen:
		DisplayServer.window_set_mode(DisplayServer.WINDOW_MODE_WINDOWED)
		fullscreen = false
		
func _ready():
	GameController.keys["None"] = true
