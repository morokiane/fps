extends Node

enum {
	ARG_INT,
	ARG_STRING,
	ARG_BOOL,
	ARG_FLOAT
}

const valid_commands = [
	["set_light", [ARG_INT]],
	["set_move", [ARG_BOOL]],
	["set_jump",[ARG_FLOAT]],
	["set_grav",[ARG_FLOAT]],
	["set_coin",[ARG_INT]],
	["set_speed",[ARG_FLOAT]],
	["set_ammo",[ARG_INT]],
	["set_plus",[ARG_BOOL]],
	["set_bp",[ARG_BOOL]],
	["help",[ARG_STRING]],
]

#export var moveSpeed: float = 80
#export var accel: float = 900
#export var frict: float = 600.0 # lower more slippery surface
#export var climbSpeed: float = 50
#export var jheight: float = -85
#export var jrelease: float = -70
#export var fallSpeed: float = 3 # adds to gravity
#export var grav: float = 200
#export var canLosespeedility: bool = true
#export var speedilityLosePercent: float = 6

func _ready():
	pass

func set_speed(speed):
	speed = float(speed)
	
	if speed >= 1 and speed <= 10:
		GameController.player.speed = speed
		return str(" Successfully set speed to ", speed)
	return "Speed value must be between 1 and 10!"

func set_jump(jump):
	jump = float(jump)
	
	GameController.player.jHeight = jump
	return str(" Successfully set jump to ", jump)

func set_grav(gravity):
	gravity = float(gravity)
	
	GameController.player.grav = gravity
	return str(" Successfully set gravity to ", gravity)

func set_light(light):
	light = int(light)
	if light == 1:
		GameController.hasFlashLight = true
	elif light == 0:
		GameController.hasFlashLight = false
	return str(" You can see in the dark ", light)

func set_move(move):
	move = bool(move)
	GameController.player.canMove = move
	return str(" Move set to  ", move)
	
#func set_speed(speed):
#	speed = float(speed)
#	GameController.player.speed = speed
#	return str(" Speed set to: ", speed)
	
func set_ammo(num):
	num = int(num)
	GameController.pistolAmmo = num
	return str(" Added ", num, " keys")
	
func set_plus(plus):
	plus = bool(plus)
	if !GameController.speedPotPlus:
		GameController.speedPotPlus = true
	else:
		GameController.speedPotPlus = false
		
	GameController.hud.speedPlus()
	return str(" speedilizer Plus is ", plus)
	
func set_bp(bp):
	bp = bool(bp)
	GameController.hasBackPack = true
	GameController.maxspeedPots = 6
	return str(" Pack rat")
	
func help(command):
#	command = str(command)
	return str(valid_commands)

