extends MeshInstance3D


@onready var texture = preload("res://textures/walls-switches/sw3s1.png")

@export var action = "interact"

func _ready():
	self.get_mesh().get("surface_0/material").set_texture(BaseMaterial3D.TEXTURE_ALBEDO, texture)
