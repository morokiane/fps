extends Node3D

func _on_area_3d_body_entered(body):
	if body is Player:
		if GameController.batteryLevel < 10:
			GameController.batteryLevel += 2
			queue_free()
		else:
			print(GameController.batteryLevel)
			return
