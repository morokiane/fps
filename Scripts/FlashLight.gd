extends Node3D

func _on_area_3d_body_entered(body):
	if body is Player:
		GameController.hasFlashLight = true
		GameController.player.flashLightOn = true
		GameController.player.timer.start()
		GameController.player.flashLight.show()
		queue_free()
