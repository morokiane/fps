extends Node3D

@export_enum("RedKey", "BlueKey", "YellowKey") var keyType: String

func _on_area_3d_body_entered(body):
	if body is Player:
		GameController.keys[keyType] = true #adds the key variable to the gamecontroller array keys
		queue_free()
		print (GameController.keys)
		
