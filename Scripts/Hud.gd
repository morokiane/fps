extends Control

@onready var flashLightIcon: TextureProgressBar = $MarginContainer/TextureProgressBar

func _ready():
	if !GameController.hasFlashLight:
		flashLightIcon.hide()

func _process(_delta):
	if GameController.hasFlashLight:
		flashLightIcon.show()

	flashLightIcon.value = GameController.batteryLevel
	
#	if Input.is_action_just_pressed("console") && !GameController.consoleActive:
#		# GameController.player.canMove = false
##		var console = load("res://Scripts/Console/Console.scn").instantiate()
##		add_child(console)
#		add_child(load("res://Scripts/Console/Console.scn").instantiate())
#		GameController.consoleActive = true
#		get_tree().paused = true
#	elif Input.is_action_just_pressed("console") && GameController.consoleActive:
##		GameController.player.canMove = true
#		GameController.consoleActive = false
#		get_tree().paused = false
#		get_node("Console").queue_free()
